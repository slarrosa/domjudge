package Exercici;

import java.util.Scanner;

public class Raid {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int ncasos = sc.nextInt();
		sc.nextLine();
		for (int i = 0; i < ncasos; i++) {
			String a = sc.nextLine();
			System.out.println(raid(a));
		}

	}

	public static String raid(String a) {

		{
			
			String[] split = a.split(":");
			int principiraid = 1350;
			int finalraid = 30;
			int min = Integer.parseInt(split[0]);
			int seg = Integer.parseInt(split[1]);
			int temps = (min * 60) + seg;
			if (temps < principiraid && temps > finalraid) {
				return (principiraid - temps + "");
			} else {
				return "RAID";
			}
		}

	}

}
