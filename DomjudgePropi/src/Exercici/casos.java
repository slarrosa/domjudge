package Exercici;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class casos {

	@Test
	void test() {
		assertEquals("1200", Raid.raid("2:30"));
		assertEquals("RAID", Raid.raid("22:47"));
		assertEquals("597", Raid.raid("12:33"));
		assertEquals("RAID", Raid.raid("00:29"));
		assertEquals("1", Raid.raid("22:29"));
		assertEquals("RAID", Raid.raid("22:30"));
		assertEquals("RAID", Raid.raid("00:00"));
		assertEquals("RAID", Raid.raid("23:59"));
		assertEquals("679", Raid.raid("11:11"));
		assertEquals("RAID", Raid.raid("00:30"));
		assertEquals("1319", Raid.raid("00:31"));

	}

}
